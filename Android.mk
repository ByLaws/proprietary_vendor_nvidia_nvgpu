LOCAL_PATH := $(call my-dir)

ifneq ($(filter switch switch_nvgpu,$(TARGET_DEVICE)),)

include $(CLEAR_VARS)
LOCAL_MODULE := libEGL_tegra
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/egl/libEGL_tegra.so
LOCAL_SRC_FILES_32 := proprietary/lib/egl/libEGL_tegra.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_RELATIVE_PATH := egl
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libGLESv1_CM_tegra
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/egl/libGLESv1_CM_tegra.so
LOCAL_SRC_FILES_32 := proprietary/lib/egl/libGLESv1_CM_tegra.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_RELATIVE_PATH := egl
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libGLESv2_tegra
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/egl/libGLESv2_tegra.so
LOCAL_SRC_FILES_32 := proprietary/lib/egl/libGLESv2_tegra.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_RELATIVE_PATH := egl
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libnvglsi
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/libnvglsi.so
LOCAL_SRC_FILES_32 := proprietary/lib/libnvglsi.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libnvwsi
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/libnvwsi.so
LOCAL_SRC_FILES_32 := proprietary/lib/libnvwsi.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libnvos
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/libnvos.so
LOCAL_SRC_FILES_32 := proprietary/lib/libnvos.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libnvgr
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/libnvgr.so
LOCAL_SRC_FILES_32 := proprietary/lib/libnvgr.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libnvrm
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/libnvrm.so
LOCAL_SRC_FILES_32 := proprietary/lib/libnvrm.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libphs
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/libphs.so
LOCAL_SRC_FILES_32 := proprietary/lib/libphs.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libnvrmapi_tegra
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/libnvrmapi_tegra.so
LOCAL_SRC_FILES_32 := proprietary/lib/libnvrmapi_tegra.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libnvrm_gpu
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/libnvrm_gpu.so
LOCAL_SRC_FILES_32 := proprietary/lib/libnvrm_gpu.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libnvrm_graphics
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/libnvrm_graphics.so
LOCAL_SRC_FILES_32 := proprietary/lib/libnvrm_graphics.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libglcore
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/libglcore.so
LOCAL_SRC_FILES_32 := proprietary/lib/libglcore.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := vulkan.tegra
LOCAL_MODULE_OWNER := nvidia
LOCAL_SRC_FILES_64 := proprietary/lib64/hw/vulkan.tegra.so
LOCAL_SRC_FILES_32 := proprietary/lib/hw/vulkan.tegra.so
LOCAL_MULTILIB := both
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_PREBUILT)
endif
