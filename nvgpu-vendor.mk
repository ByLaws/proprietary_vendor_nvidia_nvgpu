PRODUCT_COPY_FILES := \
    vendor/nvidia/nvgpu/proprietary/etc/permissions/com.nvidia.nvsi.xml:system/etc/permissions/com.nvidia.nvsi.xml \
    frameworks/native/data/etc/android.hardware.opengles.aep.xml:system/etc/permissions/android.hardware.opengles.aep.xml \
    frameworks/native/data/etc/android.hardware.vulkan.level-1.xml:system/etc/permissions/android.hardware.vulkan.level.xml \
    frameworks/native/data/etc/android.hardware.vulkan.version-1_0_3.xml:system/etc/permissions/android.hardware.vulkan.version.xml \
    $(PRODUCT_COPY_FILES)

PRODUCT_PROPERTY_OVERRIDES += \
    ro.hwui.texture_cache_size=86 \
    ro.hwui.layer_cache_size=56 \
    ro.hwui.r_buffer_cache_size=8 \
    ro.hwui.path_cache_size=40 \
    ro.hwui.gradient_cache_size=1 \
    ro.hwui.drop_shadow_cache_size=6 \
    ro.hwui.texture_cache_flushrate=0.4 \
    ro.hwui.text_small_cache_width=1024 \
    ro.hwui.text_small_cache_height=1024 \
    ro.hwui.text_large_cache_width=2048 \
    ro.hwui.text_large_cache_height=1024 \
    ro.hwui.disable_scissor_opt=true \
    ro.hardware.vulkan=tegra \
    persist.tegra.decompression=disabled \
    persist.tegra.nvblit.engine=gpu \
    persist.tegra.0x523dc5=0x3f000000 \
    persist.tegra.58027529=0x00000002 \
    persist.tegra.a3456abe=0x087f6080 \
    persist.tegra.compression=off \
    ro.sf.disable_triple_buffer=1

PRODUCT_PACKAGES += \
    libEGL_tegra \
    libGLESv1_CM_tegra \
    libGLESv2_tegra \
    vulkan.tegra \
    libnvglsi \
    libnvwsi \
    libnvos \
    libnvgr \
    libnvrm \
    libphs \
    libnvrmapi_tegra \
    libnvrm_gpu \
    libnvrm_graphics \
    libglcore \
    gralloc.gbm \
    hwcomposer.drm
